/*
  included some comments
*/
/*create table if not exists gustemp(temp_col string);

truncate table gustemp;

insert into gustemp values ('Hello, World!');
insert into gustemp values ('Hello, World!');
insert into gustemp values ('Hello, World!');
insert into gustemp values ('Hello, World!');
insert into gustemp values ('Hello, World!');
insert into gustemp values ('Hello, World!');
insert into gustemp values ('Hello, World!');
insert into gustemp values ('Hello, World!');
insert into gustemp values ('Hello, World!');*/

create table if not exists TRxSum(trx_sum_val NUMBER(10,0));

INSERT INTO TRxSum 

SELECT
SUM(VALUE) as trx_sum_val
FROM DUMMY_INGEST
Where TRX_FLAG = 1;
